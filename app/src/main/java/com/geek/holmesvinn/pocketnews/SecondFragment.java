package com.geek.holmesvinn.pocketnews;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;


public class SecondFragment extends Fragment implements Callback{

    private View view2;
    private ProgressBar progressBar;
    private RecyclerView mRecyclerView;
    private MyRecyclerViewAdapter mRecyclerViewAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view2 = inflater.inflate(R.layout.second_fragment, container, false);
        mRecyclerView = (RecyclerView) view2. findViewById(R.id.second_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        progressBar = (ProgressBar)view2.findViewById(R.id.second_progress_bar);
        Category category = (Category) getArguments().getSerializable("category");
        updateUI(category);
        return view2;
    }

    @Override
    public void onFinishDownload(ArrayList<FeedItem> feedList, Integer result) {
        progressBar.setVisibility(View.GONE);
        if(result == 1) {
            if(mRecyclerViewAdapter != null){
                mRecyclerViewAdapter.setFeedItemList(feedList);
                mRecyclerViewAdapter.notifyDataSetChanged();
                return;
            }
            mRecyclerViewAdapter = new MyRecyclerViewAdapter(getContext(), feedList);
            mRecyclerView.setAdapter(mRecyclerViewAdapter);
        }
        else {
            Toast.makeText(getContext(), "Fetch Failed!", Toast.LENGTH_LONG).show();
        }
    }

    public void updateUI(Category category){
        String url = null;
        if(category != null) {
            switch (category) {
                case BUSINESS:
                    url = getString(R.string.url_business_second);
                    break;
                case ENTERTAINMENT:
                    url = getString(R.string.url_entertainment_second);
                    break;
                case GAMING:
                    url = getString(R.string.url_gaming_second);
                    break;
                case GENERAL:
                    url = getString(R.string.url_general_second);
                    break;
                case MUSIC:
                    url = getString(R.string.url_music_second);
                    break;
                case SPORTS:
                    url = getString(R.string.url_sports_second);
                    break;
                case TECHNOLOGY:
                    url = getString(R.string.url_tech_second);
                    break;
            }
        }
        new DownloadTask(this).execute(url);
        progressBar.setVisibility(View.VISIBLE);
    }
}
