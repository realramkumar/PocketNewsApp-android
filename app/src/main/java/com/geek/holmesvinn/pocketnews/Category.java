package com.geek.holmesvinn.pocketnews;

/**
 * Created by RAMKUMAR B on 25-11-2016.
 */

public enum Category {
    GENERAL,BUSINESS,ENTERTAINMENT,GAMING,MUSIC,SPORTS,TECHNOLOGY
}
