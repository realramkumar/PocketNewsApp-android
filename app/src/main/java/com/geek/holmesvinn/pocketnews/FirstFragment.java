package com.geek.holmesvinn.pocketnews;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;


public class FirstFragment extends Fragment implements Callback{

    private View view1;
    private ProgressBar progressBar;
    private RecyclerView mRecyclerView;
    private MyRecyclerViewAdapter mRecyclerViewAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view1 = inflater.inflate(R.layout.first_fragment, container, false);
        mRecyclerView = (RecyclerView) view1. findViewById(R.id.first_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        progressBar = (ProgressBar)view1.findViewById(R.id.first_progress_bar);
        Category category = (Category) getArguments().getSerializable("category");
        updateUI(category);
        return view1;
    }

    @Override
    public void onFinishDownload(ArrayList<FeedItem> feedList, Integer result) {
        progressBar.setVisibility(View.GONE);
        if(result == 1) {
            mRecyclerViewAdapter = new MyRecyclerViewAdapter(getContext(), feedList);
            mRecyclerView.setAdapter(mRecyclerViewAdapter);
        }
        else {
            Toast.makeText(getContext(), "Fetch Failed!", Toast.LENGTH_LONG).show();
        }
    }

    public void updateUI(Category category){
        String url = null;
        if(category != null) {
            switch (category) {
                case BUSINESS:
                    url = getString(R.string.url_business_first);
                    break;
                case ENTERTAINMENT:
                    url = getString(R.string.url_entertainment_first);
                    break;
                case GAMING:
                    url = getString(R.string.url_gaming_first);
                    break;
                case GENERAL:
                    url = getString(R.string.url_general_first);
                    break;
                case MUSIC:
                    url = getString(R.string.url_music_first);
                    break;
                case SPORTS:
                    url = getString(R.string.url_sports_first);
                    break;
                case TECHNOLOGY:
                    url = getString(R.string.url_tech_first);
                    break;
            }
        }
        new DownloadTask(this).execute(url);
        progressBar.setVisibility(View.VISIBLE);
    }
}
