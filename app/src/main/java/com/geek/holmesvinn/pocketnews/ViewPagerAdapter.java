package com.geek.holmesvinn.pocketnews;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private final int tabCount = 3;
    private Category category;
    private FirstFragment firstFragment;
    private SecondFragment secondFragment;
    private ThirdFragment thirdFragment;

    public ViewPagerAdapter(FragmentManager fm, Category category) {
        super(fm);
        this.category = category;

    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle;
        switch (position) {
            case 0:
                firstFragment = new FirstFragment();
                bundle = new Bundle();
                bundle.putSerializable("category", category);
                firstFragment.setArguments(bundle);
                return firstFragment;
            case 1:
                secondFragment = new SecondFragment();
                bundle = new Bundle();
                bundle.putSerializable("category", category);
                secondFragment.setArguments(bundle);
                return secondFragment;
            case 2:
                thirdFragment = new ThirdFragment();
                bundle = new Bundle();
                bundle.putSerializable("category", category);
                thirdFragment.setArguments(bundle);
                return thirdFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (category) {
            case BUSINESS:
                switch (position) {
                    case 0:
                        return "Business Insider";
                    case 1:
                        return "Fortune";
                    case 2:
                        return "Die Zeit";
                }
            case ENTERTAINMENT:
                switch (position) {
                    case 0:
                        return "Buzz Feed";
                    case 1:
                        return "Mashable";
                    case 2:
                        return "Lad Bible";
                }
            case GAMING:
                switch (position) {
                    case 0:
                        return "IGN Top";
                    case 1:
                        return "Polygon";
                    case 2:
                        return "IGN Latest";
                }
            case GENERAL:
                switch (position) {
                    case 0:
                        return "BBC News";
                    case 1:
                        return "CNN News";
                    case 2:
                        return "Reuters";
                }
            case MUSIC:
                switch (position) {
                    case 0:
                        return "MTV Top";
                    case 1:
                        return "MTV UK";
                    case 2:
                        return "MTV Latest";
                }
            case SPORTS:
                switch (position) {
                    case 0:
                        return "BBC Sports";
                    case 1:
                        return "ESPN Cricket";
                    case 2:
                        return "Fox Sports";
                }
            case TECHNOLOGY:
                switch (position) {
                    case 0:
                        return "Hacker News";
                    case 1:
                        return "TechRadar";
                    case 2:
                        return "TechCrunch";
                }

        }
        return null;
    }
}
