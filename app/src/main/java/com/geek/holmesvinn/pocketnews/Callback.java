package com.geek.holmesvinn.pocketnews;


import java.util.ArrayList;

public interface Callback {
    public void onFinishDownload(ArrayList<FeedItem> feedList, Integer result);
}
