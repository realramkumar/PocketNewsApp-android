package com.geek.holmesvinn.pocketnews;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    TabLayout tabLayout;
    ViewPager viewPager;
    Category category;
    ViewPagerAdapter adapter;
    MenuItem mMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.navigate);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        category = Category.GENERAL;
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), category);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        //mNavigationView.getMenu().findItem(R.id.nav_item_general).setChecked(true);
        //mMenuItem = mNavigationView.getMenu().getItem(R.id.nav_item_general);
        mNavigationView.setNavigationItemSelectedListener(this);


        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

    }


    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        mDrawerLayout.closeDrawers();
        if(mMenuItem!=null)
            mMenuItem.setChecked(false);
        menuItem.setChecked(true);

        if (menuItem.getItemId() == R.id.nav_item_general) {
            category = Category.GENERAL;
        }
        if (menuItem.getItemId() == R.id.nav_item_business) {
            category = Category.BUSINESS;
        }
        if (menuItem.getItemId() == R.id.nav_item_sports) {
            category = Category.SPORTS;
        }
        if (menuItem.getItemId() == R.id.nav_item_entertainment) {
            category = Category.ENTERTAINMENT;
        }
        if (menuItem.getItemId() == R.id.nav_item_tech) {
            category =Category.TECHNOLOGY;
        }
        if (menuItem.getItemId() == R.id.nav_item_music) {
            category = Category.MUSIC;
        }
        if (menuItem.getItemId() == R.id.nav_item_game) {
            category = Category.GAMING;
        }
        if (menuItem.getItemId() == R.id.nav_menu_about){
            Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
            startActivity(intent);
        }
        mMenuItem = menuItem;
        adapter.setCategory(category);
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        return false;
    }
}