package com.geek.holmesvinn.pocketnews;

/**
 * Created by vikky on 11/22/2016.
 */
public class FeedItem {


    private String title;
    private String thumbnail;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
